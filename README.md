# oauth2token

Obtains OAuth 2.0 tokens

## Usage

```
$ oauth2token [OPTIONS] --grant-type <GRANT_TYPE> --client-id <CLIENT_ID> --client-secret <CLIENT_SECRET> <AUTH_URL>

Arguments:
  <AUTH_URL>  Authentication URL

Options:
  -g, --grant-type <GRANT_TYPE>        OAuth2 grant type [possible values: client_credentials]
  -i, --client-id <CLIENT_ID>          Client ID
  -s, --client-secret <CLIENT_SECRET>  Client Secret
  -r, --response                       Print the whole response instead of just token
  -h, --help                           Print help
  -V, --version                        Print version
```

## License

GPLv3
