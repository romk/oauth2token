//      oauth2token: obtains OAuth 2.0 tokens
//      Copyright (C) 2023  Roman Kindruk

// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.

use clap::{arg, command, ArgAction};
use std::time::Duration;
use ureq::serde_json::Value;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = command!()
        .arg(
            arg!(-g --"grant-type" <GRANT_TYPE> "OAuth2 grant type")
                .required(true)
                .value_parser(["client_credentials"]),
        )
        .arg(arg!(-i --"client-id" <CLIENT_ID> "Client ID").required(true))
        .arg(arg!(-s --"client-secret" <CLIENT_SECRET> "Client Secret").required(true))
        .arg(
            arg!(-r --response "Print the whole response instead of just token")
                .required(false)
                .action(ArgAction::SetTrue),
        )
        .arg(arg!(<AUTH_URL> "Authentication URL").required(true))
        .get_matches();

    let client_id = matches.get_one::<String>("client-id").unwrap();
    let client_secret = matches.get_one::<String>("client-secret").unwrap();
    let auth_url = matches.get_one::<String>("AUTH_URL").unwrap();
    let show_response = matches.get_flag("response");

    let resp = ureq::post(auth_url)
        .timeout(Duration::from_secs(15))
        .send_form(&[
            ("client_id", client_id),
            ("client_secret", client_secret),
            ("grant_type", "client_credentials"),
        ])?;

    if show_response {
        println!("{}", resp.into_string().unwrap());
    } else {
        let json = resp.into_json::<Value>()?;
        let token = json.get("access_token").unwrap().as_str().unwrap();
        println!("{token}");
    }
    Ok(())
}
